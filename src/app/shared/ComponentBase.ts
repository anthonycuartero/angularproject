
declare const $: any;
import * as signalR from '@microsoft/signalr';
export class ComponentBase {

    changeInterval = null;
    keyword:string='';

    showProgress(isProcessing: boolean) {
        if (isProcessing) {
            $('.divLoading').addClass("show");
        }
        else {
            $('.divLoading').removeClass("show");
        }
    }

    hubConnectionBuilder(){
       let connection = new signalR.HubConnectionBuilder()
        .withUrl("http://host.docker.internal:8086/hubs/crudhub")
        .build();

        return connection;
    }

}