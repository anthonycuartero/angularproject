import { HttpHeaders } from '@angular/common/http';
import { OnDestroy, Directive } from '@angular/core';
@Directive()
export class ServiceBase {
    baseUrl = '';
    loginUrl = '';
    controlTowerUrl = '';
    serviceId = '';
    private _standardHttpOptions = {};
    private _urlEncodeHttpOptions = {};
    private _uploadProgressHttpOptions = {};
    private subscription;
    get standardHttpOptions(): {} {
        this.initCurrentUser();
        return this._standardHttpOptions;
    }
    get urlEncodeHttpOptions(): {} {
        this.initCurrentUser();
        return this._urlEncodeHttpOptions;
    }
    get uploadProgressHttpOptions(): {} {
        this.initCurrentUser();
        return this._uploadProgressHttpOptions;
    }
    constructor() {
        this.getBaseUrl(); //AppConfig.settings.apiServer;
        
        this._standardHttpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            })
        };
        this._urlEncodeHttpOptions = {
            headers: new HttpHeaders({
                "Content-Type": "application/x-www-form-urlencoded",
                "Access-Control-Allow-Origin": "*"
            })
        };
        this._uploadProgressHttpOptions = {
            reportProgress: true,
            observe: 'events',
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            })
        };
    }

    private getBaseUrl() {
        // this.baseUrl = `http://52.139.244.214:8080/`;
        // this.baseUrl = `https://localhost:44332/`; 8086
        this.baseUrl = `http://host.docker.internal:8086/`
    }

    initCurrentUser() {
        var accessToken = localStorage.getItem('token');

        if (accessToken != null) {
            this._standardHttpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Authorization': 'Bearer ' + accessToken
                })
            };
            this._urlEncodeHttpOptions = {
                headers: new HttpHeaders({
                    'Access-Control-Allow-Origin': '*',
                    "Content-Type": "application/x-www-form-urlencoded",
                    'Authorization': 'Bearer ' + accessToken
                })
            };

            this._uploadProgressHttpOptions = {
                reportProgress: true,
                observe: 'events',
                headers: new HttpHeaders({
                    'Access-Control-Allow-Origin': '*',
                    'Authorization': 'Bearer ' + accessToken
                })
            };
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
