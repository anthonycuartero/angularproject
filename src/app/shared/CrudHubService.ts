import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root',
})
export class CrudHubService{
    
    onInsertData(data:any, crudData:any){
        var data = JSON.parse(JSON.stringify(data));
        crudData.push(data);
    }
    
    onUpdateData(data:any, crudData:any){
        var data = JSON.parse(JSON.stringify(data));
        var subject = crudData.find(x => x.id == data.id);
        let index = crudData.indexOf(subject);
        crudData[index] = data;
    }
    
    onRemoveData(data:any, crudData:any){
        var data = JSON.parse(JSON.stringify(data));
        var subject = crudData.find(x => x.id == data);
        let index = crudData.indexOf(subject);
        crudData.splice(index, 1);
    }
}