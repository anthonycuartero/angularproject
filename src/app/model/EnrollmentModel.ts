import { Guid } from "guid-typescript";

export class EnrollmentModel{
    id:Guid;
    userId:string='';
    courseId:string='';
    createdAt:Date = new Date();
    updatedAt:Date = new Date();
    courseName:string='';
    courseDescription:string='';
}