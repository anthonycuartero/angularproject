import { Guid } from "guid-typescript";

export class ModuleModel{
    id:Guid;
    title:string='';
    duration:number=0;
    courseId:string='';
    contents:string='';
    isPublished:number=0;
    courseTitle:string='';
    createdAt:Date=new Date();
    updatedAt:Date=new Date();
    version:number=0;
}