import { Guid } from "guid-typescript";

export class UserModel{
    id:Guid;
    email:string='';
    role:string='';
    firstName:string='';
    lastName:string='';
}