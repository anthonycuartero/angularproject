import { Guid } from "guid-typescript";

export class ContentModel{
    id:Guid;
    moduleId:string='';
    contentName:string='';
    content:string='';
    isPublished:number=0;
    createdAt:Date = new Date();
    updatedAt:Date = new Date();
}