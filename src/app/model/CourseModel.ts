import { Guid } from "guid-typescript";

export class CourseModel{
    id:Guid;
    subjectId:string='';
    title:string='';
    description:string='';
    icon:string='';
    author:string='';
    duration:string='';
    isPublished:number=0;
    modules:string[]=[];
    createdAt:Date = new Date();
    updatedAt:Date = new Date();
}