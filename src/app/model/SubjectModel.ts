import { Guid } from "guid-typescript";

export class SubjectModel{
    id:Guid;
    title:string='';
    courses:string[]=[];
    isPublished:number=0;
    owner:string='';
    createdAt:Date = new Date();
    updatedAt:Date = new Date();
}