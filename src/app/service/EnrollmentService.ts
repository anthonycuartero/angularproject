import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ContentModel } from '../model/ContentModel';
import { CourseModel } from '../model/CourseModel';
import { EnrollmentModel } from '../model/EnrollmentModel';
import { ModuleModel } from '../model/ModuleModel';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class EnrollmentService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getEnrollmentList(studentId){
    let params = new HttpParams();
    params = params.append('Student', studentId);

    return this.http.get(this.baseUrl+'enrollments', {headers: this.standardHttpOptions,params: params});
  }

  getEnrollmentDetails(id:string){
    return this.http.get(this.baseUrl+`enrollments/${id}`, this.standardHttpOptions);
  }

  insertEnrollment(content:EnrollmentModel){
    return this.http.post(this.baseUrl+`enrollments`, content, this.standardHttpOptions);
  }

  updateEnrollment(content:EnrollmentModel){
    var id = content.id;
    return this.http.patch(this.baseUrl+`enrollments/${id}`, content, this.standardHttpOptions);
  }

  deleteEnrollment(id:string){
    return this.http.delete(this.baseUrl+`enrollments/${id}`, this.standardHttpOptions);
  }

}