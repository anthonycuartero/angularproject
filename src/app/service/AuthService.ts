import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class AuthService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  // readonly loginURL = "https://localhost:44359/";
  // readonly loginURL = "http://52.139.244.214:8083/";

  readonly loginURL = "http://host.docker.internal:8085/";


  login(formData:any){

    const body = new HttpParams()
    .set('username', formData.username)
    .set('password', formData.password)
    .set('grant_type', 'password')
    .set('client_id', 'postman')
    .set('client_secret', 'secret')
    .set('scope', 'apiscope');

    return this.http.post(this.loginURL+'connect/token', body, this.urlEncodeHttpOptions);

  }
  
  register(formData:any){
    return this.http.post(this.baseUrl+'signup', formData, this.standardHttpOptions);
  }

  externalLogin(formData:any){
    return this.http.post(this.baseUrl+'externallogin', formData, this.standardHttpOptions);
  }
  

}