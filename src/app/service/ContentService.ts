import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ContentModel } from '../model/ContentModel';
import { CourseModel } from '../model/CourseModel';
import { ModuleModel } from '../model/ModuleModel';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class ContentService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getContentList(moduleId, keyword){
    let params = new HttpParams();
    params = params.append('Module', moduleId);
    params = params.append('Keyword', keyword);

    return this.http.get(this.baseUrl+'contents', {headers: this.standardHttpOptions,params: params});
  }

  getContentDetails(id:string){
    return this.http.get(this.baseUrl+`contents/${id}`, this.standardHttpOptions);
  }

  insertContent(content:ContentModel){
    return this.http.post(this.baseUrl+`contents`, content, this.standardHttpOptions);
  }

  updateContent(content:ContentModel){
    var id = content.id;
    return this.http.patch(this.baseUrl+`contents/${id}`, content, this.standardHttpOptions);
  }

  deleteContent(id:string){
    return this.http.delete(this.baseUrl+`contents/${id}`, this.standardHttpOptions);
  }

}