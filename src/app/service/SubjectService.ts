import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubjectModel } from '../model/SubjectModel';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class SubjectService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getSubjectList(keyword:string){
    let params = new HttpParams();
    params = params.append('Keyword', keyword);
    
    return this.http.get(this.baseUrl+'subjects', {headers: this.standardHttpOptions,params: params});
  }

  getSubjectDetails(id:string){
    return this.http.get(this.baseUrl+`subjects/${id}`, this.standardHttpOptions);
  }

  insertSubject(subject:SubjectModel){
    return this.http.post(this.baseUrl+`subjects`, subject, this.standardHttpOptions);
  }

  updateSubject(subject:SubjectModel){
    var id = subject.id;
    return this.http.patch(this.baseUrl+`subjects/${id}`, subject, this.standardHttpOptions);
  }

  deleteSubject(id:string){
    return this.http.delete(this.baseUrl+`subjects/${id}`, this.standardHttpOptions);
  }

}