import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CourseModel } from '../model/CourseModel';
import { ModuleModel } from '../model/ModuleModel';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class ModuleService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getModuleList(courseId, keyword){
    let params = new HttpParams();
    params = params.append('Course', courseId);
    params = params.append('Keyword', keyword);

    return this.http.get(this.baseUrl+'modules', {headers: this.standardHttpOptions,params: params});
  }

  getModuleDetails(id:string){
    return this.http.get(this.baseUrl+`modules/${id}`, this.standardHttpOptions);
  }

  insertModule(module:ModuleModel){
    return this.http.post(this.baseUrl+`modules`, module, this.standardHttpOptions);
  }

  updateModule(module:ModuleModel){
    var id = module.id;
    return this.http.patch(this.baseUrl+`modules/${id}`, module, this.standardHttpOptions);
  }

  deleteModule(id:string){
    return this.http.delete(this.baseUrl+`modules/${id}`, this.standardHttpOptions);
  }

}