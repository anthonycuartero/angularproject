import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class UserService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getUsersList(){
    return this.http.get(this.baseUrl+'users', this.standardHttpOptions);
  }

  getUserDetails(id:string){
    return this.http.get(this.baseUrl+`users/${id}`, this.standardHttpOptions);
  }

  getUserDetailsByToken(){
    return this.http.get(this.baseUrl+`user`, this.standardHttpOptions);
  }

}