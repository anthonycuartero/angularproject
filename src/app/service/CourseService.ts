import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CourseModel } from '../model/CourseModel';
import { ServiceBase } from '../shared/ServiceBase';

@Injectable({
  providedIn: 'root',
})
export class CourseService extends ServiceBase {

  constructor(private http:HttpClient) {
    super();
  }

  getCourseList(subjectId, keyword:string){
    let params = new HttpParams();
    params = params.append('SubjectId', subjectId);
    params = params.append('Keyword', keyword);

    return this.http.get(this.baseUrl+'courses', {headers: this.standardHttpOptions,params: params});
  }

  getCourseDetails(id:string){
    return this.http.get(this.baseUrl+`courses/${id}`, this.standardHttpOptions);
  }

  insertCourse(course:CourseModel){
    return this.http.post(this.baseUrl+`courses`, course, this.standardHttpOptions);
  }

  updateCourse(course:CourseModel){
    var id = course.id;
    return this.http.patch(this.baseUrl+`courses/${id}`, course, this.standardHttpOptions);
  }

  deleteCourse(id:string){
    return this.http.delete(this.baseUrl+`courses/${id}`, this.standardHttpOptions);
  }

}