import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ADMIN: RouteInfo[] = [
    { path: '/subjects', title: 'Subjects',  icon: 'ni-tv-2 text-primary', class: '' },
    // { path: '/user-profile', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    { path: '/users', title: 'Users',  icon:'ni-bullet-list-67 text-red', class: '' },
];

export const STUDENTS: RouteInfo[] = [
  { path: '/student-course', title: 'Courses',  icon: 'ni-tv-2 text-primary', class: '' },
  { path: '/student-enrolled-course', title: 'My Courses',  icon:'ni-single-02 text-yellow', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {

    var role = localStorage.getItem('role');

    if(role == 'Instructor' || role == 'Admin'){
      var navRoutes = ADMIN;
    }else{
      var navRoutes = STUDENTS;
    }


    this.menuItems = navRoutes.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
