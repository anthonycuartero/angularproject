import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseModel } from 'src/app/model/CourseModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { CourseService } from 'src/app/service/CourseService';
import { ComponentBase } from 'src/app/shared/ComponentBase';
import { CrudHubService } from 'src/app/shared/CrudHubService';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent extends ComponentBase implements OnInit {

  courseList: CourseModel[] = [];
  courseDetail: CourseModel = new CourseModel();
  selectedDetails:string='CourseLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  subjectId:string='';

  constructor(private courseService:CourseService,
    private currentRoute: ActivatedRoute,
    private crudHubService: CrudHubService) {
      super();
     }

  ngOnInit() {
    this.subjectId = this.currentRoute.snapshot.paramMap.get('subjectId');
    console.log(this.courseDetail);
    console.log(this.subjectId);
    this.courseDetail.subjectId = this.subjectId;
    this.getCourseList();

    let connection = this.hubConnectionBuilder();
    this.setConnections(connection);


    connection.start().then((data: any) => {
      connection.invoke('AddToGroup', "Course")
      .catch((error: any) => {
        alert(error);
      });
    });

  }

  setConnections(connection:any){
    connection.on("InsertData", data => {
      this.crudHubService.onInsertData(data, this.courseList);
    });
    connection.on("RemoveData", data => {
      this.crudHubService.onRemoveData(data, this.courseList);
    });
    connection.on("UpdateData", data => {
      this.crudHubService.onUpdateData(data, this.courseList)
    });
  }


  onSearchCourse(){
    clearInterval(this.changeInterval)
    this.changeInterval = setInterval(() => {
      this.getCourseList();
      clearInterval(this.changeInterval);
    }, 1000);
  }

  getCourseList(){
    this.courseService.getCourseList(this.subjectId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.courseList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  onAddCourse(){
    this.selectedDetails='AddCourse';
    this.courseDetail = new CourseModel();
  }

  addCourse(){
    this.courseDetail.subjectId = this.subjectId;
    this.courseService.insertCourse(this.courseDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'CourseLists';
        this.courseDetail = new CourseModel();
        alert("Successfully added");
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }


  onUpdate(course:CourseModel){
    this.courseDetail = course;
    this.selectedDetails = 'AddCourse';
  }

  updateCourse(){
    this.courseService.updateCourse(this.courseDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'CourseLists';
        this.courseDetail = new CourseModel();
        alert("Successfully updated")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  deleteCourse(id:string){
    this.courseService.deleteCourse(id).subscribe(
      (res:any)=>{
        alert("Successfully deleted")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }



  selectPagination(value:number, selected:string){
    this.offset = value;
    this.selectedPagination = selected;
    this.getCourseList();
  }
  previousPagination(){
    if(this.pagination.one == 1){
      return;
    }
    this.pagination.one -= 5;
    this.pagination.two -= 5;
    this.pagination.three -= 5;
    this.pagination.four -= 5;
    this.pagination.five -= 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getCourseList();
  }
  nextPagination(){
    this.pagination.one += 5;
    this.pagination.two += 5;
    this.pagination.three += 5;
    this.pagination.four += 5;
    this.pagination.five += 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getCourseList();
  }


}
