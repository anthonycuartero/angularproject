import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModuleModel } from 'src/app/model/ModuleModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { ModuleService } from 'src/app/service/ModuleService';
import { ComponentBase } from 'src/app/shared/ComponentBase';
import { CrudHubService } from 'src/app/shared/CrudHubService';


@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent extends ComponentBase implements OnInit {

  moduleList: ModuleModel[] = [];
  moduleDetail: ModuleModel = new ModuleModel();
  selectedDetails:string='ModuleLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  courseId:string='';
  subjectId:string='';

  constructor(private moduleService:ModuleService,
    private currentRoute: ActivatedRoute,
    private crudHubService: CrudHubService) { super(); }

  ngOnInit() {
    this.courseId = this.currentRoute.snapshot.paramMap.get('courseId');
    this.subjectId = this.currentRoute.snapshot.paramMap.get('subjectId');
    console.log(this.moduleDetail);
    console.log(this.courseId);
    this.moduleDetail.courseId = this.courseId;
    this.getModuleList();

    let connection = this.hubConnectionBuilder();
    this.setConnections(connection);


    connection.start().then((data: any) => {
      connection.invoke('AddToGroup', "Module")
      .catch((error: any) => {
        alert(error);
      });
    });
  }

  setConnections(connection:any){
    connection.on("InsertData", data => {
      this.crudHubService.onInsertData(data, this.moduleList);
    });
    connection.on("RemoveData", data => {
      this.crudHubService.onRemoveData(data, this.moduleList);
    });
    connection.on("UpdateData", data => {
      this.crudHubService.onUpdateData(data, this.moduleList)
    });
  }


  onSearchModule(){
    clearInterval(this.changeInterval)
    this.changeInterval = setInterval(() => {
      this.getModuleList();
      clearInterval(this.changeInterval);
    }, 1000);
  }

  getModuleList(){
    this.moduleService.getModuleList(this.courseId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.moduleList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  onAddModule(){
    this.selectedDetails='AddModule';
    this.moduleDetail = new ModuleModel();
  }

  addModule(){
    this.moduleDetail.courseId = this.courseId;
    this.moduleService.insertModule(this.moduleDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'ModuleLists';
        this.moduleDetail = new ModuleModel();
        alert("Successfully added");
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }


  onUpdate(module:ModuleModel){
    this.moduleDetail = module;
    this.selectedDetails = 'AddModule';
  }

  updateModule(){
    this.moduleService.updateModule(this.moduleDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'ModuleLists';
        this.moduleDetail = new ModuleModel();
        alert("Successfully updated")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  deleteModule(id:string){
    this.moduleService.deleteModule(id).subscribe(
      (res:any)=>{
        alert("Successfully deleted")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }



  selectPagination(value:number, selected:string){
    this.offset = value;
    this.selectedPagination = selected;
    this.getModuleList();
  }
  previousPagination(){
    if(this.pagination.one == 1){
      return;
    }
    this.pagination.one -= 5;
    this.pagination.two -= 5;
    this.pagination.three -= 5;
    this.pagination.four -= 5;
    this.pagination.five -= 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getModuleList();
  }
  nextPagination(){
    this.pagination.one += 5;
    this.pagination.two += 5;
    this.pagination.three += 5;
    this.pagination.four += 5;
    this.pagination.five += 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getModuleList();
  }


}
