import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentModel } from 'src/app/model/ContentModel';
import { ModuleModel } from 'src/app/model/ModuleModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { ContentService } from 'src/app/service/ContentService';
import { ModuleService } from 'src/app/service/ModuleService';
import { ComponentBase } from 'src/app/shared/ComponentBase';


@Component({
  selector: 'app-student-modules',
  templateUrl: './student-modules.component.html',
  styleUrls: ['./student-modules.component.scss']
})
export class StudentModulesComponent extends ComponentBase implements OnInit {

  moduleList: ModuleModel[] = [];
  moduleDetail: ModuleModel = new ModuleModel();

  contentList: ContentModel[] = [];

  selectedDetails:string='ModuleLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  courseId:string='';
  subjectId:string='';

  constructor(private moduleService:ModuleService,
    private contentService:ContentService,
    private currentRoute: ActivatedRoute) { super(); }

  ngOnInit() {
    this.courseId = this.currentRoute.snapshot.paramMap.get('courseId');
    this.subjectId = this.currentRoute.snapshot.paramMap.get('subjectId');
    console.log(this.moduleDetail);
    console.log(this.courseId);
    this.moduleDetail.courseId = this.courseId;
    this.getModuleList();
  }

  getModuleList(){
    this.moduleService.getModuleList(this.courseId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.moduleList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  getContentList(moduleId){
    this.contentService.getContentList(moduleId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.contentList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }
}
