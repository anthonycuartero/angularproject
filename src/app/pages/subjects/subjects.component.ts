import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { SubjectModel } from 'src/app/model/SubjectModel';
import { SubjectService } from 'src/app/service/SubjectService';
import { ComponentBase } from 'src/app/shared/ComponentBase';

import * as signalR from '@microsoft/signalr';
import { CrudHubService } from 'src/app/shared/CrudHubService';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent extends ComponentBase implements OnInit {

  subjectList: SubjectModel[] = [];
  subjectDetail: SubjectModel = new SubjectModel();
  selectedDetails:string='SubjectLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  constructor(private subjectService:SubjectService,
    private crudHubService: CrudHubService) {
    super();
   }

  ngOnInit() {
    this.getSubjectLists();

    let connection = this.hubConnectionBuilder();
    this.setConnections(connection);


    connection.start().then((data: any) => {
      connection.invoke('AddToGroup', "Subject")
      .catch((error: any) => {
        alert(error);
      });
    });
  }

  setConnections(connection:any){
    connection.on("InsertData", data => {
      this.crudHubService.onInsertData(data, this.subjectList);
    });
    connection.on("RemoveData", data => {
      this.crudHubService.onRemoveData(data, this.subjectList);
    });
    connection.on("UpdateData", data => {
      this.crudHubService.onUpdateData(data, this.subjectList)
    });
  }



  onSearchSubject(){
    clearInterval(this.changeInterval)
    this.changeInterval = setInterval(() => {
      this.getSubjectLists();
      clearInterval(this.changeInterval);
    }, 1000);
  }

  getSubjectLists(){
    this.showProgress(true);
    this.subjectService.getSubjectList(this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.subjectList = data;
        this.showProgress(false);
      },
      err=>{
        this.showProgress(false);
        if(err.status == 400){

        }
      }
    );
  }

  onAddSubject(){
    this.selectedDetails='AddSubject';
    this.subjectDetail = new SubjectModel();
  }

  addSubject(){
    this.subjectService.insertSubject(this.subjectDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'SubjectLists';
        this.subjectDetail = new SubjectModel();
        alert("Successfully added");
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  onUpdate(subject:SubjectModel){
    this.subjectDetail = subject;
    this.selectedDetails = 'AddSubject';
  }

  updateSubject(){
    this.subjectService.updateSubject(this.subjectDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'SubjectLists';
        this.subjectDetail = new SubjectModel();
        alert("Successfully updated")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  deleteSubject(id:string){
    this.subjectService.deleteSubject(id).subscribe(
      (res:any)=>{
        alert("Successfully deleted")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }
}
