import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentModel } from 'src/app/model/ContentModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { ContentService } from 'src/app/service/ContentService';
import { ComponentBase } from 'src/app/shared/ComponentBase';
import { CrudHubService } from 'src/app/shared/CrudHubService';


@Component({
  selector: 'app-contents',
  templateUrl: './contents.component.html',
  styleUrls: ['./contents.component.scss']
})
export class ContentsComponent extends ComponentBase implements OnInit {

  contentList: ContentModel[] = [];
  contentDetail: ContentModel = new ContentModel();
  selectedDetails:string='ContentLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  courseId:string='';
  moduleId:string='';
  subjectId:string='';

  constructor(private contentService:ContentService,
    private currentRoute: ActivatedRoute,
    private crudHubService : CrudHubService) { super(); }

  ngOnInit() {
    this.courseId = this.currentRoute.snapshot.paramMap.get('courseId');
    this.moduleId = this.currentRoute.snapshot.paramMap.get('moduleId');
    this.subjectId = this.currentRoute.snapshot.paramMap.get('subjectId');
    this.contentDetail.moduleId = this.moduleId;
    this.getContentList();

    let connection = this.hubConnectionBuilder();
    this.setConnections(connection);


    connection.start().then((data: any) => {
      connection.invoke('AddToGroup', "Content")
      .catch((error: any) => {
        alert(error);
      });
    });
  }

  setConnections(connection:any){
    connection.on("InsertData", data => {
      this.crudHubService.onInsertData(data, this.contentList);
    });
    connection.on("RemoveData", data => {
      this.crudHubService.onRemoveData(data, this.contentList);
    });
    connection.on("UpdateData", data => {
      this.crudHubService.onUpdateData(data, this.contentList)
    });
  }

  onSearchContent(){
    clearInterval(this.changeInterval)
    this.changeInterval = setInterval(() => {
      this.getContentList();
      clearInterval(this.changeInterval);
    }, 1000);
  }

  getContentList(){
    this.contentService.getContentList(this.moduleId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.contentList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  onAddContent(){
    this.selectedDetails='AddContent';
    this.contentDetail = new ContentModel();
  }

  addContent(){
    this.contentDetail.moduleId = this.moduleId;
    this.contentDetail.content = this.contentDetail.contentName;
    this.contentService.insertContent(this.contentDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'ContentLists';
        this.contentDetail = new ContentModel();
        alert("Successfully added");
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }


  onUpdate(content:ContentModel){
    this.contentDetail = content;
    this.selectedDetails = 'AddContent';
  }

  updateContent(){
    this.contentDetail.content = this.contentDetail.contentName;
    this.contentService.updateContent(this.contentDetail).subscribe(
      (res:any)=>{
        this.selectedDetails = 'ContentLists';
        this.contentDetail = new ContentModel();
        alert("Successfully updated")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  deleteContent(id:string){
    this.contentService.deleteContent(id).subscribe(
      (res:any)=>{
        alert("Successfully deleted")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }



  selectPagination(value:number, selected:string){
    this.offset = value;
    this.selectedPagination = selected;
    this.getContentList();
  }
  previousPagination(){
    if(this.pagination.one == 1){
      return;
    }
    this.pagination.one -= 5;
    this.pagination.two -= 5;
    this.pagination.three -= 5;
    this.pagination.four -= 5;
    this.pagination.five -= 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getContentList();
  }
  nextPagination(){
    this.pagination.one += 5;
    this.pagination.two += 5;
    this.pagination.three += 5;
    this.pagination.four += 5;
    this.pagination.five += 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getContentList();
  }


}
