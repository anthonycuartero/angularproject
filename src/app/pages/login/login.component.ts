import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/model/UserModel';
import { AuthService } from 'src/app/service/AuthService';
import { UserService } from 'src/app/service/UserService';
import { ComponentBase } from 'src/app/shared/ComponentBase';

import { SocialAuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends ComponentBase implements OnInit, OnDestroy {
  userDetail: UserModel = new UserModel();
  constructor(private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private socialAuthService: SocialAuthService) {
    super();
  }

  formModel = {
    username: '',
    password: '',
    grant_type: 'password',
    client_id: 'postman',
    client_secret: 'secret',
    scope: 'apiscope',
  }

  fbModel = {
    authToken: '',
    id: '',
    provider: '',
    firstName: '',
    lastName: '',
    email: '',
    photoUrl: '',
  }

  userData: any[] = [];
  resultMessage: string;

  ngOnInit() {
    if (localStorage.getItem('token') != null) {
      if (localStorage.getItem('token') == 'Student') {
        this.router.navigateByUrl('/student-course');
      } else {
        this.router.navigateByUrl('/subjects');
      }
    }
  }
  ngOnDestroy() {
  }

  logInWithExternal(platform:string) {
    var providerId = FacebookLoginProvider.PROVIDER_ID;
    if(platform == "Google"){
      providerId = GoogleLoginProvider.PROVIDER_ID
    }
    this.socialAuthService.signIn(providerId).then(
      (response) => {
        this.fbModel = response;
        if(response){
          this.externalLogin();
        }
      },
      (error) => {
        console.log(error);
        this.resultMessage = error;
      }
    )
  }

  externalLogin(){
    this.showProgress(true);
    this.authService.externalLogin(this.fbModel).subscribe(
      (res: any) => {
        console.log(res);
        localStorage.setItem('token', res.access_token)
        this.getUserDetails();
        this.showProgress(false);
      },
      err => {
        this.showProgress(false);
        if (err.status == 400) {

        }
      }
    );
  }

  onLogin() {
    this.showProgress(true);
    this.authService.login(this.formModel).subscribe(
      (res: any) => {
        localStorage.setItem('token', res.access_token)
        console.log("successfully login");
        this.getUserDetails();
        this.showProgress(false);
      },
      err => {
        this.showProgress(false);
        if (err.status == 400) {

        }
      }
    );
  }

  getUserDetails() {
    console.log("getUserDetails");
    this.userService.getUserDetailsByToken().subscribe(
      (res: any) => {
        this.userDetail = JSON.parse(JSON.stringify(res));
        localStorage.setItem('role', this.userDetail.role);
        localStorage.setItem('fullname', this.userDetail.firstName +' '+ this.userDetail.lastName);
        alert("Successfully login");
        if (this.userDetail.role == 'Student') {
          this.router.navigateByUrl('/student-course');
        } else {
          this.router.navigateByUrl('/subjects');
        }

      },
      err => {
        if (err.status == 400) {

        }
      }
    );
  }
}
