import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/app/model/UserModel';
import { UserService } from 'src/app/service/UserService';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  userList: UserModel[]=[];
  userDetail: UserModel= new UserModel();

  constructor(private userService:UserService) { }

  ngOnInit() {
    this.getUsersList();

  }

  getUsersList(){
    this.userService.getUsersList().subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.userList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  getUserDetails(id:string){
    this.userService.getUserDetails(id).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res));
        this.userDetail = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

}
