import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentEnrolledCourseComponent } from './student-enrolled-course.component';

describe('StudentEnrolledCourseComponent', () => {
  let component: StudentEnrolledCourseComponent;
  let fixture: ComponentFixture<StudentEnrolledCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentEnrolledCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentEnrolledCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
