import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Guid } from 'guid-typescript';
import { CourseModel } from 'src/app/model/CourseModel';
import { EnrollmentModel } from 'src/app/model/EnrollmentModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { CourseService } from 'src/app/service/CourseService';
import { EnrollmentService } from 'src/app/service/EnrollmentService';
import { ComponentBase } from 'src/app/shared/ComponentBase';


@Component({
  selector: 'app-student-enrolled-course',
  templateUrl: './student-enrolled-course.component.html',
  styleUrls: ['./student-enrolled-course.component.scss']
})
export class StudentEnrolledCourseComponent extends ComponentBase implements OnInit {

  enrollmentList: CourseModel[] = [];
  enrollmentDetail: CourseModel = new CourseModel();
  selectedDetails:string='EnrollmentLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  subjectId:string='';

  constructor(private enrollmentService:EnrollmentService,
    private currentRoute: ActivatedRoute) { super(); }

  ngOnInit() {
    this.getEnrollmentList();
  }


  getEnrollmentList(){
    var subjectId = Guid.createEmpty();
    this.enrollmentService.getEnrollmentList(subjectId).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.enrollmentList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  onAddCourse(){
    this.enrollmentDetail = new CourseModel();
  }


  deleteCourse(id:string){
    this.enrollmentService.deleteEnrollment(id).subscribe(
      (res:any)=>{
        this.getEnrollmentList();
        alert("Successfully deleted")
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }


}
