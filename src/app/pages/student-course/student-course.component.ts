import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Guid } from 'guid-typescript';
import { CourseModel } from 'src/app/model/CourseModel';
import { EnrollmentModel } from 'src/app/model/EnrollmentModel';
import { PaginationModel } from 'src/app/model/PaginationModel';
import { CourseService } from 'src/app/service/CourseService';
import { EnrollmentService } from 'src/app/service/EnrollmentService';
import { ComponentBase } from 'src/app/shared/ComponentBase';


@Component({
  selector: 'app-student-course',
  templateUrl: './student-course.component.html',
  styleUrls: ['./student-course.component.scss']
})
export class StudentCourseComponent extends ComponentBase implements OnInit {

  courseList: CourseModel[] = [];
  enrollmentDetail: EnrollmentModel = new EnrollmentModel();
  selectedDetails:string='CourseLists';

  pagination:PaginationModel = new PaginationModel();
  selectedPagination:string = 'one';
  offset:number = 1;

  subjectId:string='';

  constructor(private courseService:CourseService,
    private enrollmentService:EnrollmentService,
    private currentRoute: ActivatedRoute) { super(); }

  ngOnInit() {
    this.getCourseList();
  }

  onSearchCourse(){
    clearInterval(this.changeInterval)
    this.changeInterval = setInterval(() => {
      this.getCourseList();
      clearInterval(this.changeInterval);
    }, 1000);
  }

  getCourseList(){
    var subjectId = Guid.createEmpty();
    this.courseService.getCourseList(subjectId, this.keyword).subscribe(
      (res:any)=>{
        var data = JSON.parse(JSON.stringify(res.data));
        this.courseList = data;
      },
      err=>{
        if(err.status == 400){

        }
      }
    );
  }

  enrollCourse(course: CourseModel){
    this.enrollmentDetail.courseId = course.id.toString();
    this.enrollmentService.insertEnrollment(this.enrollmentDetail).subscribe(
      (res:any)=>{
        alert("Successfully enrolled")
      },
      (err:any)=>{
        var data = JSON.parse(JSON.stringify(err.error));
        console.log(data);
        alert(data.message);
      }
    );
  }


  selectPagination(value:number, selected:string){
    this.offset = value;
    this.selectedPagination = selected;
    this.getCourseList();
  }
  previousPagination(){
    if(this.pagination.one == 1){
      return;
    }
    this.pagination.one -= 5;
    this.pagination.two -= 5;
    this.pagination.three -= 5;
    this.pagination.four -= 5;
    this.pagination.five -= 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getCourseList();
  }
  nextPagination(){
    this.pagination.one += 5;
    this.pagination.two += 5;
    this.pagination.three += 5;
    this.pagination.four += 5;
    this.pagination.five += 5;
    this.selectedPagination = 'one';
    this.offset = this.pagination.one;
    this.getCourseList();
  }


}
