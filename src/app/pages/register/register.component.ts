import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/AuthService';
import { ComponentBase } from 'src/app/shared/ComponentBase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends ComponentBase implements OnInit {

  constructor(private authSerivce:AuthService,
    private fb:FormBuilder,
    private router: Router) {
      super();
     }

  ngOnInit() {
    if(localStorage.getItem('token') != null){
      this.router.navigateByUrl("/subjects");
    }
  }

  formModel = this.fb.group({
    Email:['', Validators.email],
    Role:['Instructor'],
    FirstName:[''],
    LastName:[''],
    Passwords: this.fb.group({
      Password:['', Validators.required, Validators.minLength(6)],
      ConfirmPassword:['', Validators.required]
    }, {validator: this.comparePasswords})
  })

  comparePasswords(fb:FormGroup){
    var confirmPswrdCtrl = fb.get('ConfirmPassword');
    if(confirmPswrdCtrl?.errors == null || 'passwordMismatch' in confirmPswrdCtrl?.errors){
      if(fb.get('Password')?.value != confirmPswrdCtrl?.value){
        confirmPswrdCtrl?.setErrors({passwordMismatch:true});
      }else{
        confirmPswrdCtrl?.setErrors(null);
      }
    }
  }

  onRegister(){
    this.showProgress(true);
    var body = {
      Email: this.formModel.value.Email,
      Password: this.formModel.value.Passwords.Password,
      VerifyPassword: this.formModel.value.Passwords.ConfirmPassword,
      Role: this.formModel.value.Role,
      FirstName: this.formModel.value.FirstName,
      LastName: this.formModel.value.LastName,
    }

    this.authSerivce.register(body).subscribe(
      (res:any) =>{
        if(res){
          this.formModel.reset();
          alert("Successfuly register");
          this.router.navigateByUrl('/login');
        }
        this.showProgress(false);
      },
      err => {
        this.showProgress(false);
        console.log(err);
      }
      
    );
  }

}
