import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { SubjectsComponent } from '../../pages/subjects/subjects.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { UsersComponent } from '../../pages/users/users.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoursesComponent } from 'src/app/pages/courses/courses.component';
import { ModulesComponent } from 'src/app/pages/modules/modules.component';
import { ContentsComponent } from 'src/app/pages/contents/contents.component';
import { StudentCourseComponent } from 'src/app/pages/student-course/student-course.component';
import { StudentEnrolledCourseComponent } from 'src/app/pages/student-enrolled-course/student-enrolled-course.component';
import { StudentModulesComponent } from 'src/app/pages/student-modules/student-modules.component';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule
  ],
  declarations: [
    SubjectsComponent,
    UserProfileComponent,
    UsersComponent,
    IconsComponent,
    MapsComponent,
    CoursesComponent,
    ModulesComponent,
    ContentsComponent,
    StudentCourseComponent,
    StudentEnrolledCourseComponent,
    StudentCourseComponent,
    StudentModulesComponent
  ]
})

export class AdminLayoutModule {}
