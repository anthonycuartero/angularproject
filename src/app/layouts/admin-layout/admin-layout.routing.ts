import { Routes } from '@angular/router';

import { SubjectsComponent } from '../../pages/subjects/subjects.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { UsersComponent } from '../../pages/users/users.component';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { CoursesComponent } from 'src/app/pages/courses/courses.component';
import { ModulesComponent } from 'src/app/pages/modules/modules.component';
import { ContentsComponent } from 'src/app/pages/contents/contents.component';
import { StudentCourseComponent } from 'src/app/pages/student-course/student-course.component';
import { StudentEnrolledCourseComponent } from 'src/app/pages/student-enrolled-course/student-enrolled-course.component';
import { StudentGuard } from 'src/app/auth/student.guard';
import { StudentModulesComponent } from 'src/app/pages/student-modules/student-modules.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'subjects',      component: SubjectsComponent, canActivate:[AuthGuard]},
    { path: 'user-profile',   component: UserProfileComponent, canActivate:[AuthGuard] },
    { path: 'courses/:subjectId',   component: CoursesComponent, canActivate:[AuthGuard] },
    { path: 'modules/:courseId/:subjectId',   component: ModulesComponent, canActivate:[AuthGuard] },
    { path: 'contents/:moduleId/:courseId/:subjectId',   component: ContentsComponent, canActivate:[AuthGuard] },
    { path: 'users',         component: UsersComponent, canActivate:[AuthGuard] },
    { path: 'icons',          component: IconsComponent, canActivate:[AuthGuard] },
    { path: 'maps',           component: MapsComponent, canActivate:[AuthGuard] },

    
    { path: 'student-course',   component: StudentCourseComponent, canActivate:[StudentGuard] },
    { path: 'student-enrolled-course',   component: StudentEnrolledCourseComponent, canActivate:[StudentGuard] },
    { path: 'student-modules/:courseId',   component: StudentModulesComponent, canActivate:[StudentGuard] },
    
];
